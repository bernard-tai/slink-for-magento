<?php
/**
 * Slink for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Slink_MageSaasu
 * @package    Sales
 * @copyright  Copyright (c) 2009 Bernard Tai
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard.tai@saaslink.net
 */
	
class Slink_MageSaasu_Admin_SalesController extends Mage_Adminhtml_Controller_Action
{
 
	protected function indexAction() {
		$this->loadLayout();
		$this->_addContent($this->getLayout()->createBlock('slink/admin_sales_list'));
        $this->getLayout()->getBlock('head')->setTitle('Sales');
		$this->renderLayout();
	}
	
	public function viewAction(){
		$this->loadLayout();
		$this->_addContent($this->getLayout()->createBlock('slink/admin_sales_view'));
        $this->getLayout()->getBlock('head')->setTitle('View Sale');
		$this->renderLayout();
	}
	public function massLinkAction(){
		$config = Mage::getStoreConfig('slinksettings');
		if($ids = $this->getRequest()->getParam('sales', false)){
			foreach($ids as $id){
				try{
					$slink_sale = Mage::getModel('slink/sales')->load($id);
					$order_no = Mage::getModel('sales/order')->load($slink_sale->getData('vid'))->getData('increment_id');
					$slink_sale->link();
					$this->_getSession()->addSuccess(Mage::helper('slink')->__('Sale '.$order_no.' linked.'));
				}catch(Exception $e){
					$this->_getSession()->addError($e->getMessage());
					continue;
				}
			}
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
	}
	public function linkAction(){	
		$config = Mage::getStoreConfig('slinksettings');
		try{			
			if ($id = $this->getRequest()->getParam('id', false)){
				$slink_sale = Mage::getModel('slink/sales')->load($id);
				$order_no = Mage::getModel('sales/order')->load($slink_sale->getData('vid'))->getData('increment_id');
				$slink_sale->link();
				$this->_getSession()->addSuccess(Mage::helper('slink')->__('Sale '.$order_no.' linked.'));
			}
		}catch (Exception $e){
			$this->_getSession()->addError($e->getMessage());
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
	}
	public function massUnlinkAction(){
		$config = Mage::getStoreConfig('slinksettings');
		if($ids = $this->getRequest()->getParam('sales', false)){
			foreach($ids as $id){
				try{

					$slink_sale = Mage::getModel('slink/sales')->load($id);
					$order_no = Mage::getModel('sales/order')->load($slink_sale->getData('vid'))->getData('increment_id');
					$slink_sale->unlink();
					$this->_getSession()->addSuccess('Sale '.$order_no.' link removed.');

				}catch(Exception $e){
					$this->_getSession()->addError($e->getMessage());
					continue;
				}
			}
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
		
	}
	public function unlinkAction(){	
		$config = Mage::getStoreConfig('slinksettings');
		try{		
			if ($id = $this->getRequest()->getParam('id', false)){
				$slink_sale = Mage::getModel('slink/sales')->load($id);
				$order_no = Mage::getModel('sales/order')->load($slink_sale->getData('vid'))->getData('increment_id');
				$slink_sale->unlink();
				$this->_getSession()->addSuccess('Sale order '.$order_no.' link removed.');
			}
		} catch (Exception $e){
			$this->_getSession()->addError($e->getMessage());
		}
		
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));		
		
	}

}