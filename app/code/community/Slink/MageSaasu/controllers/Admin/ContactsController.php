<?php
/**
 * Slink for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Slink_MageSaasu
 * @package    Contacts
 * @copyright  Copyright (c) 2009 Bernard Tai
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard.tai@saaslink.net
 */

	
class Slink_MageSaasu_Admin_ContactsController extends Mage_Adminhtml_Controller_Action
{
	protected function indexAction() {
        
		$this->loadLayout();
		$this->_addContent($this->getLayout()->createBlock('slink/admin_contacts_list'));
        $this->getLayout()->getBlock('head')->setTitle('Contacts');
		$this->renderLayout();

	}
	public function viewAction(){
		$this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('slink/admin_contacts_view'));
        $this->getLayout()->getBlock('head')->setTitle('View Contact');        
		$this->renderLayout();
	}
	
	public function massLinkAction(){
		
		$config = Mage::getStoreConfig('slinksettings');
		
		if($ids = $this->getRequest()->getParam('contacts', false)){
			foreach($ids as $id){
				try{
					
					$slink_contact = Mage::getModel('slink/contacts')->load($id);

					if($customer = Mage::getModel('customer/customer')->load($slink_contact->getData('vid')))
						$lastname = $customer->getData('lastname');

					$slink_contact->link($id);
					$this->_getSession()->addSuccess('Customer "'.$lastname.'" linked.');

				}catch(Exception $e){
					Mage::getSingleton('adminhtml/session')->addError('Customer "'.$lastname.'" - '.$e->getMessage);
					continue;
				}
			}
			
		}
		
		$this->_redirect('*/*/');

	}

	public function linkAction(){
        $id = $this->getRequest()->getParam('id');
        
		$config = Mage::getStoreConfig('slinksettings');
		try{	
            $slink_contact = Mage::getModel('slink/contacts')->load($id);
			
			if($customer = Mage::getModel('customer/customer')->load($slink_contact->getData('vid')))
				$lastname = $customer->getData('lastname');
			
			$slink_contact->link();

			$this->_getSession()->addSuccess('Customer "'.$lastname.'" linked.');

		}catch(Exception $e){
			$this->_getSession()->addError('Customer "'.$lastname.'" - '.$e->getMessage());
		}

		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
	}
	
	public function unlinkAction(){
		$config = Mage::getStoreConfig('slinksettings');
		$id = $this->getRequest()->getParam('id', false);
		
		if($id){
			try{
				$slink_contact = Mage::getModel('slink/contacts')->load($id);
				
				if($customer = Mage::getModel('customer/customer')->load($slink_contact->getData('vid')))
					$lastname = $customer->getData('lastname');
				
				$slink_contact->unlink();
				
				$this->_getSession()->addSuccess('Customer "'.$lastname.'" link removed');

			}catch(Exception $e){
				$this->_getSession()->addError('Customer "'.$lastname.'" - '.$e->getMessage());
			}
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
	}
	

	
	public function massUnlinkAction(){
		$config = Mage::getStoreConfig('slinksettings');
		if($ids = $this->getRequest()->getParam('contacts', false)){
			foreach($ids as $id){
				try{

					$slink_contact = Mage::getModel('slink/contacts')->load($id);
					if($customer = Mage::getModel('customer/customer')->load($slink_contact->getData('vid')))
						$lastname = $customer->getData('lastname');
					
					$slink_contact->unlink();
					$this->_getSession()->addSuccess('Customer "'.$lastname.'" link removed');
				}catch(Exception $e){
					$this->_getSession()->addError('Customer "'.$lastname.'" - '.$e->getMessage());
				}
			}
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
	}
		

}