<?php
/**
 * Slink for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Slink_MageSaasu
 * @package    Items
 * @copyright  Copyright (c) 2009 Bernard Tai
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard.tai@saaslink.net
 */

class Slink_MageSaasu_Admin_ItemsController extends Mage_Adminhtml_Controller_Action
{   

	protected function indexAction() {        
		$this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('slink/admin_items_list'));
        $this->getLayout()->getBlock('head')->setTitle('Items');
        $this->renderLayout();
	}
	public function viewAction(){
		$this->loadLayout();
		$this->_addContent($this->getLayout()->createBlock('slink/admin_items_view'));
        $this->getLayout()->getBlock('head')->setTitle('View Item');
		$this->renderLayout();
	}	
	public function massLinkAction(){
		$config = Mage::getStoreConfig('slinksettings');
		if($ids = $this->getRequest()->getParam('items', false)){
			foreach($ids as $id){
				try{
					
					$slink_item = Mage::getModel('slink/items')->load($id);
					if($product = Mage::getModel('catalog/product')->load($slink_item->getData('vid')))
						$sku = $product->getSku();
					
					$slink_item->link();
					$this->_getSession()->addSuccess('Item '.$sku.' linked.');
				}catch(Exception $e){
					$this->_getSession()->addError('Item '.$sku.' - '.$e->getMessage());
					continue;
				}
			}
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));		
	}
	
	public function linkAction(){
		$id = $this->getRequest()->getParam('id', false);
		$config = Mage::getStoreConfig('slinksettings');
		try{
			$slink_item = Mage::getModel('slink/items')->load($id);
			if($product = Mage::getModel('catalog/product')->load($slink_item->getData('vid')))
				$sku = $product->getSku();
			$slink_item->link();
			$this->_getSession()->addSuccess('Item '.$sku.' linked.');
		}catch(Exception $e){
			$this->_getSession()->addError('Item '.$sku.' - '.$e->getMessage());
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
		
	}
	public function massUnlinkAction(){
		$config = Mage::getStoreConfig('slinksettings');
		if($ids = $this->getRequest()->getParam('items', false)){
			foreach($ids as $id){
				try{
					$slink_item = Mage::getModel('slink/items')->load($id);
					if($product = Mage::getModel('catalog/product')->load($slink_item->getData('vid')))
						$sku = $product->getSku();
					$slink_item->unlink();
					$this->_getSession()->addSuccess('Item '.$sku.' link removed.');
				}catch(Exception $e){
					$this->_getSession()->addError('Item '.$sku.' - '.$e->getMessage());
					continue;
				}
			}
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
		
	}
	public function unlinkAction(){
		$config = Mage::getStoreConfig('slinksettings');
		try{
			if($id = $this->getRequest()->getParam('id', false)){
				$slink_item = Mage::getModel('slink/items')->load($id);
				$sku = Mage::getModel('catalog/product')->load($slink_item->getData('vid'))->getSku();
				$slink_item->unlink();
				$this->_getSession()->addSuccess('Item '.$sku.' link removed.');
			}
		}catch(Exception $e){
			$this->_getSession()->addError('Item '.$sku.' - '.$e->getMessage());
		}
		$this->getResponse()->setRedirect($this->getUrl('*/*/'));
	}	

}