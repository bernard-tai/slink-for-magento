<?php

class Slink_MageSaasu_Block_Admin_Sales_View extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{		
		parent::__construct();
		
		$this->_blockGroup = 'slink';
		$this->_controller = 'admin_sales';
		$this->_mode = 'view';
		
		if( $id = $this->getRequest()->getParam($this->_objectId) ) {
			
			$collection = Mage::getModel('slink/sales')->getCollection()->addFieldToFilter('id', $id);
			$collection->getSelect()->joinLeft(array('sales'=>Mage::getModel('sales/order')->getResource()->getMainTable()),
											   'sales.entity_id = main_table.vid',
											   array('sales.*'));
			$sale = $collection->getFirstItem();

			$config = Mage::getStoreConfig('slinksettings');
			$tags = ($config['sales']['saleTags'] ? explode(' , ', $config['sales']['saleTags']): array());
			if($sale->getData('coupon_code')) $tags[] = $sale->getData('coupon_code');
			
			$billing = Mage::getModel('sales/order_address')->load($sale->getData('billing_address_id'));
			$shipping = Mage::getModel('sales/order_address')->load($sale->getData('shipping_address_id'));

			Mage::register('current_sale', $sale->getData());
			Mage::register('sale_tags', implode(',', $tags));
			Mage::register('billing_name', $billing->getData('lastname').', '.$billing->getData('firstname'));
			Mage::register('shipping_name', $shipping->getData('lastname').', '.$shipping->getData('firstname'));
		}
		$this->_removeButton('save');
		$this->_removeButton('delete');			
		$this->_removeButton('reset');			

	}			
		
	public function getHeaderText()
	{
		return Mage::helper('slink')->__("View Sale");			
	}
}
