<?php

	class Slink_MageSaasu_Block_Admin_Items_View extends Mage_Adminhtml_Block_Widget_Form_Container
	{
		public function __construct()
		{	
			parent::__construct();
			
			$this->_blockGroup = 'slink';
			$this->_mode = 'view';
			$this->_controller = 'admin_items';
						
			if( $id = $this->getRequest()->getParam($this->_objectId) ) {
				
				$collection = Mage::getModel('slink/items')->getCollection()->addFieldToFilter('id', $id);
				$collection->getSelect()->joinLeft(array('catalog_product'=>Mage::getModel('core/resource')->getTableName('catalog/product')),
												   'catalog_product.entity_id = main_table.vid',
												   array('catalog_product.*'));
				$item = $collection->getFirstItem();
				$product = Mage::getModel('catalog/product')->load($item->getData('entity_id'));

				$item = array_merge($item->getData(), $product->getData());
				Mage::register('current_item', $item);
				
				$stock_data = $item['stock_item'];
				Mage::register('current_stock_info', $stock_data->getData());
			}
			
			$this->_removeButton('save');
			$this->_removeButton('delete');			
			$this->_removeButton('reset');						
		}			
			
		
		public function getHeaderText()
		{
			return Mage::helper('slink')->__("View Item");			
		}
	}
