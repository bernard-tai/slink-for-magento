<?php
	class Slink_MageSaasu_Block_Admin_Contacts_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
	{
		public function __construct(){
			parent::__construct();
			$this->setId('slink_magesaasu_block_admin_contacts_list_grid');
			$this->_controller = 'slink';
		}

		protected function _prepareCollection(){
            
            $collection = Mage::getModel('slink/contacts')->getCollection();
			
			$customer_entity_type = Mage::getModel('eav/entity_type')->loadByCode('customer');
			$address_entity_type = Mage::getModel('eav/entity_type')->loadByCode('customer_address');
			
			$collection->getSelect()->joinLeft(array('customer'=>$collection->getTable($customer_entity_type->getEntityTable())),
											   'customer.entity_id = main_table.vid',
											   array('*'));
			
			$this->setDefaultSort('created_at');
			$this->setDefaultDir('DESC');
			$this->setCollection($collection);
			

			return parent::_prepareCollection();
		}
		protected function _prepareMassaction(){
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('contacts');
			$this->getMassactionBlock()->addItem(	'link', array('label'=>Mage::helper('slink')->__('Link'),
																  'url'=>$this->getUrl('*/*/massLink')));
			$this->getMassactionBlock()->addItem(	'unlink', array('label'=>Mage::helper('slink')->__('Unlink'),
																  'url'=>$this->getUrl('*/*/massUnlink')));
			
		}
		
		protected function _prepareColumns(){
			$this->addColumn(	'vid', array(
											 'header'        => Mage::helper('slink')->__('Customer ID'),
											 'width'         => '30px',
											 'index'         => 'vid',
											 'type'          => 'text',
											 'escape'        => true
											 ));
			
			$this->addColumn(	'contact', array(		'header'        => Mage::helper('slink')->__('Name'),
														'align'         => 'left',
														'width'         => '500px',
														'index'         => 'name',
														'type'          => 'text',
														'renderer'		=> 'slink/admin_contacts_list_column_renderer_contactname'
												 ));
			
			$this->addColumn(	'status', array(	'header'		=> Mage::helper('slink')->__('Status'),
													'align'		=> 'center',
													'width'		=> '50px',
													'index'		=> 'transferred',
													'filter'	=> false,
													'renderer'=>	'slink/admin_contacts_list_column_renderer_status'
											 ));			
			$this->addColumn(	'transferred', array(	'header'        => Mage::helper('slink')->__('Linked at (yyyy-mm-dd)'),
														'align'         => 'left',
														'width'			=> '150px',
														'index'         => 'transferred',
														'type'          => 'text',
														'escape'        => false,
														'renderer'		=> 'slink/admin_slink_list_column_renderer_date'
											 ));

			$this->addColumn('actions', array(
								   'header'    => Mage::helper('slink')->__('Action'),
								   'width'     => '150px',
								   'type'      => 'action',
								   'getter'	=> 'getId',
								   'actions'   => array(array('caption' => Mage::helper('slink')->__('Link'),
															  'url'     => array('base'=>'*/*/link'),
															  'field'   => 'id' ),
														array('caption' => Mage::helper('slink')->__('Unlink'),
															  'url'     => array('base'=>'*/*/unlink'),
															  'field'   => 'id' ),														
														'filter'    => false,
														'sortable'  => false)));
			return parent::_prepareColumns();
		}
		
		public function getRowUrl($row)
		{
//			return $this->getUrl('*/*/view', array('id' => $row->getId()));
		}		
	}
