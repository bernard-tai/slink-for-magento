<?php
	class Slink_MageSaasu_Block_Admin_Contacts_List_Column_Renderer_Contactname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
	{
		public function render(Varien_Object $row){
			
			$customer = Mage::getModel('customer/customer')->load($row->getData('vid'));
			$billing = $customer->getDefaultBillingAddress();
			if($billing) $name = $billing->getName();
			else $name = $customer->getName();
			return ($customer->getId() ? $name.(!$customer->getData('is_active') ? '(inactive)' : '') : '[Deleted]');
		}
	}
