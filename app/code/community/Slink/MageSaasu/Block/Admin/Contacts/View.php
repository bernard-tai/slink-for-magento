<?php

	class Slink_MageSaasu_Block_Admin_Contacts_View extends Mage_Adminhtml_Block_Widget_Form_Container
	{
		public function __construct()
		{	

			parent::__construct();
			
			$this->_blockGroup = 'slink';
			$this->_mode = 'view';
			$this->_controller = 'admin_contacts';
		
			if( $id = $this->getRequest()->getParam($this->_objectId) ) {
				
				$collection = Mage::getModel('slink/contacts')->getCollection()->addFieldToFilter('id', $id);
				$collection->getSelect()->joinLeft(array('customer_order_address'=>Mage::getSingleton('core/resource')->getTableName('customer/address_entity')),
												   'customer_order_address.entity_id = main_table.vid',
												   array('customer_order_address.*'));
				
				$item = $collection->getFirstItem();
				$address = Mage::getModel('customer/address')->load($item->getData('entity_id'));
				$customer = Mage::getModel('customer/customer')->load($address->getData('parent_id'));
				
				if($address->entity_id == $customer->default_billing){
					$address_type='Billing';
				}elseif($address->entity_id == $customer->default_shipping){
					$address_type='Shipping';
				}else{
					$address_type='Additional';
				}
				
				$data = array_merge($item->getData(), $address->getData(), array('address_type'=>$address_type));
				Mage::register('current_contact', $data);

			}
			
			$this->_removeButton('save');
			$this->_removeButton('delete');			
			$this->_removeButton('reset');						
			
/*			//Still buggy
			$this->_addButton('link', array('label'=>'Link', 
											'style'=>'width:70px',
											'class'=>'link',
											'onclick'	=> 'editForm.submit();',
											), -1);			*/
		}			
			
		
		public function getHeaderText()
		{
			return Mage::helper('slink')->__("View Customer");			
		}
	}
