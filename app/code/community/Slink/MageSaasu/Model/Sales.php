<?php
/**
 * Slink for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Slink_MageSaasu
 * @package    Sales
 * @copyright  Copyright (c) 2009 Bernard Tai
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard.tai@saaslink.net
 */

class Slink_MageSaasu_Model_Sales extends Mage_Core_Model_Abstract
{
	protected $sale; /* variable for use with internal functions */
	protected $slink_saasu_sale;	/* variable for use with internal functions */
    
	protected $saleAmount;

	protected $isGuestBilling = false;
	protected $isGuestShipping = false;
	protected $linkMessage = '';
    
    protected function _construct(){
		parent::_construct();
        $this->_init('slink/sales');		

    }
	public function link(){
		$this->linkMessage = ''; 

        $this->sale = Mage::getModel('sales/order')->load($this->getData('vid'));
        $sale = &$this->sale;
        
        if($sale->getId()>0){

			$config = Mage::getStoreConfig('slinksettings') ;						
			$this->slink_saasu_sale = Mage::getModel('slink/saasu_sale');
            $slink_saasu_sale = &$this->slink_saasu_sale;
        
            /* Look for item if UID available */
            if($this->getData('entity_uid')<>"" && $this->getData('updated_uid')<>"") $slink_saasu_sale->get($this->getData('entity_uid'));

            /* Look for Magento Order increment id (if not Auto Numbered) */
            if(!$config['sales']['saleAutoNumber']){
                if($slink_saasu_sale->getData('uid')=="" && $slink_saasu_sale->getData('lastUpdatedUid')=="" && $this->sale->getData('increment_id')>0)
                    $slink_saasu_sale->getByInvoiceNumber($config['sales']['salePrefix'].$sale->getData('increment_id'));

            }else{
				if(!$slink_saasu_sale->getData('notes')) $slink_saasu_sale->setData('notes', 'Magento Invoice Number: #'.$sale->getData('increment_id'));
			}
            
            if(($slink_saasu_sale->getData('uid')=="" && $slink_saasu_sale->getData('lastUpdatedUid')=="")
               || /* If item has been updated - post anyway */
               $this->getData('transferred') < $this->sale->getData('updated_at')){
                
                $slink_saasu_sale->setSale( $this->sale );	                                
                $slink_saasu_sale->setData('status', $config['sales']['saasuSaleStatus']);
                $slink_saasu_sale->setData('layout', $config['sales']['saleLayout']);
                /* if Layout=S or I determine how to add items here */
                
                
                if($config['sales']['saleAutoNumber']){
                    if(!$slink_saasu_sale->getData('invoiceNumber')) $slink_saasu_sale->setData('invoiceNumber', '<Auto Number>');
                }else{
                    $slink_saasu_sale->setData('invoiceNumber', $config['sales']['salePrefix'].$this->sale->getData('increment_id'));
                }
                $slink_saasu_sale->setData('tags', $config['sales']['saleTags']);
                
                
                $slink_saasu_sale->setData('ccy', $this->sale->getData('order_currency_code'));
                /* Set quick payment bank account here */
                
                $this->_setContacts();
                $this->_setItems();
                $this->_setPayments();

                if($slink_saasu_sale->post()){
                    $slink_saasu_sale->setData('transferred' , Mage::getModel('core/date')->gmtTimestamp());
                }		                
            }
			
            $errors = $slink_saasu_sale->getError();
            if(count($errors)<1 && ($entity_uid = $slink_saasu_sale->getData('uid')) && ($updated_uid = $slink_saasu_sale->getData('lastUpdatedUid'))){
                                
				$this->addData(array('entity_uid'=>$entity_uid,
									 'updated_uid'=>$updated_uid,
									 'invoice_number'=>$slink_saasu_sale->getData('invoiceNumber'),
									 'transferred'=>$slink_saasu_sale->getData('transferred')
									 ));
                
                if(!$this->getData('transferred'))
                   $this->setData('transferred', Mage::getModel('core/date')->gmtTimestamp());
                
				$this->save();
                return true;
			}else{
			
                /** delete guest contacts created in Saasu for this sale **/
				if($config['contacts']['linkGuestContacts']){
					$slink_contact = Mage::getModel('slink/saasu_contact');
					if($this->isGuestBilling){
						$slink_contact->delete($slink_saasu_sale->getData('contactUid'));
					}
					if($this->isGuestShipping){
						$slink_contact->delete($slink_saasu_sale->getData('shipToContactUid'));
					}
				}
                
				foreach($errors as $error){
					$message .= 'Sale '.$slink_saasu_sale->getData('increment_id').' - '.$error['code']. ' - '.$error['message'].($error['action'] ? ' ('.$error['action'].' )' : '').'<br>';
				}
                if($message<>"") throw new Exception($message);
                return false;
			}	
		}else return false;
				
	}
	
	public function unlink(){
		if($this->getData('id') > 0){	
			
			$this->addData(array('entity_uid'=>'',
								 'updated_uid'=>'',
								 'invoice_number'=>'',
								 'transferred'=>''));
			$this->save();
			return;
		}else throw new Exception ('No Sale with ID '.$this->getId());
			
	}
	private function _setContacts(){
		$config = Mage::getStoreConfig('slinksettings') ;
		if($this->sale && $this->slink_saasu_sale){
            
            $sale = &$this->sale;
            $slink_saasu_sale = &$this->slink_saasu_sale;
			$config = Mage::getStoreConfig('slinksettings/contacts') ;
            
			$billing = $sale->getBillingAddress();
			$shipping = $sale->getShippingAddress();
            
			$c = Mage::getModel('slink/saasu_contact');
			
			if($billing->getId()){
				$firstname = $billing->getData('firstname');
				$lastname = $billing->getData('lastname');
				$telephone = $billing->getData('telephone');
			}else{
				$firstname = $sale->getData('customer_firstname');
				$lastname = $sale->getData('customer_lastname');
			}
			
			$email = $sale->getData('customer_email');
			
			if($c->getData('uid')=="" && $c->getData('lastUpdatedUid')==""){
				$c->getByInfo($firstname,$lastname,$telephone,$email);
			}
		
			if($c->getData('uid')=="" && $c->getData('lastUpdatedUid')==""){
			
				if($billing && $billing->getId()){
					$c->setContact($billing);
					$c->addPostalAddress($billing->getData('street'),
										 $billing->getData('city'),
										 $billing->getData('region'),
										 $billing->getData('postcode'),
										 $billing->getData('country_id'));
				}
				
				if($shipping && $shipping->getId()){
					$c->addOtherAddress($shipping->getData('street'),
										$shipping->getData('city'),
										$shipping->getData('region'),
										$shipping->getData('postcode'),
										$shipping->getData('country_id'));
				}
				
				$c->setData('tags', $config['contactTags']);
				$c->setData('email', $sale->getData('customer_email'));
				
				if($c->post()){
                    $c->setData('transferred' , Mage::getModel('core/date')->gmtTimestamp());					
				}
				
				if(($errors = $c->getError()) && count($errors)>0){
					foreach($errors as $error){
						$message .= 'Contact '.$billing->getData('lastname').' ( '.$error['code']. ' - '.$error['message'].' ( '.$error['action'].' ) <br>';
					}
					if($message<>"") throw new Exception($message);
					return false;
				}
			}
			
			$slink_saasu_sale->setData('contactUid', $c->getData('uid'));
			$slink_saasu_sale->setData('notes', $slink_saasu_sale->getData('notes')."\n\n".
												'Shipping Address: '."\n".
												($shipping->getData('firstname') ? $shipping->getData('firstname')." " : "").
												$shipping->getData('lastname')."\n".
												($shipping->getData('company') ? $shipping->getData('company')."\n" : "").
												($shipping->getData('street') ? $shipping->getData('street')."\n" : "").
												($shipping->getData('city') ? $shipping->getData('city')."\n" : "").
												($shipping->getData('region') ? $shipping->getData('region')." " : "").
												($shipping->getData('country_id') ? $shipping->getData('country_id')." " : "").
												$shipping->getData('postcode')."\n");
			
		}
	}
	
	private function _setItems(){
        $sale = &$this->sale;
        $slink_saasu_sale = &$this->slink_saasu_sale;
		
		if($sale->getId()>0 && $slink_saasu_sale){
			$config = Mage::getStoreConfig('slinksettings') ;
			$items = $sale->getItemsCollection();
            
			foreach($items as $item){
				/**	Price info in Configurable product,
				 Qty info in Configurable and its Simple product */
				if($item->product_type == 'configurable'){
					continue;
				}
				/** Skip Bundle products. Price and Qty info in its Simple product */
				elseif($item->product_type == 'bundle'){
					continue;
				}else{
					if($item->parent_item_id > 0){ // Has a parent configurable product, use parent's price and qty info
						$parent = Mage::getModel('sales/order_item')->load($item->parent_item_id);
						if($parent->getId()>0 && $parent->product_type=='configurable') $i = $parent;
						else $i = $item;
					}else $i = $item;
					
					$unitPrice = $i->getData('price');
					/* Division by 0 */
					if($i->getData('qty_ordered')){
						$unitTax = $i->getData('tax_amount') / $i->getData('qty_ordered');
						$unitPriceInclTax = $i->getData('row_total_incl_tax') / $i->getData('qty_ordered');
					}else{
						$unitTax = $i->getData('tax_amount');
						$unitPriceInclTax = $i->getData('row_total_incl_tax');
					}
					
					/* Round discount to 4 decimals */
					$percentageDiscount = round(100 - (($i->getData('row_total') + $i->getData('tax_amount') - $i->getData('discount_amount')) / $i->getData('row_total_incl_tax') * 100), 4);
				
					if($percentageDiscount<0){
						$qty = $i->getData('qty_ordered')*-1;
					}else{
						$qty = $i->getData('qty_ordered');
					}

					if($product_id = Mage::getModel('catalog/product')->getIdBySku($item->getData('sku'))){
                        if(($slink_item = Mage::getModel('slink/items')->load($product_id, 'vid')) && !$slink_item->getId()){
                            $slink_item->setData(array('vid'=>$product_id));
                            $slink_item->save();
						}
                        $slink_item->link(); /* Link to ensure correct uid */
						$entity_uid = $slink_item->getData('entity_uid');
					}else{
						/* Item not found in catalog. Find item in Slink and link by SKU, else create */
						$slink_item = Mage::getModel('slink/saasu_item');
                        $slink_item->getBySku($i->getSku());
                        
						if(!$slink_item->getData('uid')) throw new Exception('Sale '.$this->sale->getData('increment_id').' - '.
                                                                             'Item '.$item->getSku().' not found.');
                        
                        $entity_uid = $slink_item->getData('uid');
					}
                    
					if($config['sales']['saleLayout']=="I"){
						$slink_saasu_sale->addItemInvoiceItem($qty,
															  $entity_uid,
															  $item->getData('name'),
															  ($i->getData('tax_amount') > 0 ? $config['tax']['saleTaxCode'] : $config['tax']['saleTaxFreeCode']), //tax code
															  //											 ($discount > 0 ? ($unitPriceInclTax / (1 - $discount)) : $unitPriceInclTax),
															  $unitPriceInclTax,
															  //											 $item->getData('discount_percent'));
															  abs($percentageDiscount));
					}else{
						$totalAmountInclTax = $item->getData('qty_ordered') * $unitPriceInclTax;
						$defaultIncomeAccountUid = $config['items']['defaultIncomeAccountUid'];
						$slink_saasu_sale->addServiceInvoiceItem($item->getData('sku'),
																 $defaultIncomeAccountUid,
																 ($i->getData('tax_amount') > 0 ? $config['tax']['saleTaxCode'] : $config['tax']['saleTaxFreeCode']),
																 $totalAmountInclTax,
																 abs($percentageDiscount));
					}
                    $this->saleAmount += ($qty * $unitPriceInclTax) * (1-($percentageDiscount/100));
				}
			}
			
			if(($sale->getData('shipping_amount') + $sale->getData('shipping_tax_amount'))>0){
				if($config['items']['shippingItemCode']=="") throw new Exception('No item code provided for Shipping. (See Slink Settings > Items).');
            
                
                $shipping_item = Mage::getModel('slink/saasu_item');
                $shipping_item->getBySku($config['items']['shippingItemCode']);
                if(!$shipping_item->getData('uid')) throw new Exception ('No shipping item found for code '.$config['items']['shippingItemCode']);
				
				$shipping_amount - $sale->getData('shipping_amount')+$sale->getData('shipping_tax_amount');
				$shipping_percentage_discount = $sale->getData('shipping_discount_amount') / $sale->getData('shipping_amount') * 100;
				
				if($config['sales']['saleLayout']=="I"){
					$slink_saasu_sale->addItemInvoiceItem(1,
														  $shipping_item->getData('uid'),
														  $shipping_item->getData('description'),
														  ($sale->getData('shipping_tax_amount') > 0 ? $config['tax']['saleTaxCode'] : $config['tax']['saleTaxFreeCode']),
														  ($sale->getData('shipping_amount')+$sale->getData('shipping_tax_amount')),
														  $shipping_percentage_discount);
				}else{
					$defaultIncomeAccountUid = $config['items']['defaultIncomeAccountUid'];
					$slink_saasu_sale->addServiceInvoiceItem($shipping_item->getData('description'),
															 $defaultIncomeAccountUid,
															 ($sale->getData('shipping_tax_amount') > 0 ? $config['tax']['saleTaxCode'] : $config['tax']['saleTaxFreeCode']),
															 ($sale->getData('shipping_amount')+$sale->getData('shipping_tax_amount')));
				}
				$this->saleAmount += ($sale->getData('shipping_amount')+$sale->getData('shipping_tax_amount')) - $sale->getData('shipping_discount_amount');
			}
			
			if(($coupon = $sale->getData('coupon_code')) <> ""){
				if($config['items']['couponItemCode']=="") throw new Exception( 'No item code provided for Coupon Discounts. (See Slink Settings > Items). ');
				
                $coupon_item = Mage::getModel('slink/saasu_item');
                $coupon_item->getBySku($config['items']['couponItemCode']);
                
                if(!$coupon_item->getData('uid')) throw new Exception ('No coupon item found for code '.$config['items']['couponItemCode']);
                   
				/** Adding coupon code to sale tags in Saasu **/
                $current_tags = $slink_saasu_sale->getData('tags');
                array_push($current_tags, $sale->getData('coupon_code'));
                $slink_saasu_sale->setData('tags', $current_tags);
                
				if($config['sales']['saleLayout']=="I"){
					$slink_saasu_sale->addItemInvoiceItem(	1,
														  $coupon_item->getData('uid'),
														  'Coupon Code "'.$sale->getData('coupon_code').'"',
														  '', //No tax code
														  0, // 0 value for line item
														  0); // 0 value for line item discount
				}else{
					$defaultIncomeAccountUid = $config['items']['defaultIncomeAccountUid'];					
					$slink_saasu_sale->addServiceInvoiceItem('Coupon Code "'.$sale->getData('coupon_code').'"',
															 $defaultIncomeAccountUid,
															 '',
															 0);
				}
            }
		}
	}
	
	private function _setPayments(){
        
        $sale = &$this->sale;
        $slink_saasu_sale = &$this->slink_saasu_sale;
        
		if($sale->getId()>0){
            
			$config = Mage::getStoreConfig('slinksettings') ;
			if($config['sales']['createQuickPayment']) {
				
				if($slink_saasu_sale->getData('uid')<>"" && /* Create Quick Payment only if not previously sent with payment */
				   $slink_saasu_sale->getData('lastUpdatedUid')<>"") {
					Mage::getSingleton('adminhtml/session')->addError('Sale '.$sale->getData('increment_id').' has already been linked. Payment creation skipped.');
					return;
				}
				
				$bankAccountUid = trim($config['sales']['bankAccountUid']);
				if($curr = $this->sale->order_currency_code){
					$regexps = @unserialize($config['sales']['bankAccountCurrencyRegExp']);
                
					foreach($regexps as $exception){
						$exception_currency = $exception['regexp'];
						if(false===strpos($exception_currency, '/', 0)) {
							$exception_currency = '/'.$exception_currency.'/';
						}
						if(@preg_match($exception_currency, $sale->order_currency_code)){
							$bankAccountUid = $exception['value'];
						}
					}
				}
				if($bankAccountUid){
                    $slink_saasu_sale->addQuickPayment( Mage::getModel('core/date')->date('Y-m-d', $sale->getData('created_at')),
                                                        Mage::getModel('core/date')->date('Y-m-d', $sale->getData('created_at')),
                                                        $bankAccountUid,
                                                        $this->saleAmount,
                                                        '',
                                                        'Online Order '.$this->sale->getData('increment_id'));
				}else{
					$this->linkMessage .= 'No Bank Account assigned to Sale Payment. See Slink Settings > Sales > Create Payment.';
				}
			}
        }
				
	}
    
	
}