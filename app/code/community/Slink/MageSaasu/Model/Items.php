<?php
/**
 * Slink for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Slink_MageSaasu
 * @package    Items
 * @copyright  Copyright (c) 2009 Bernard Tai
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard.tai@saaslink.net
 */

class Slink_MageSaasu_Model_Items extends Mage_Core_Model_Abstract
{
	
    protected function _construct()
    {
		parent::_construct();
        $this->_init('slink/items');		
    }
	
	public function link(){

        $item = Mage::getModel('catalog/product')->load($this->getData('vid'));

		if($item->getId()>0){

            $slink_item = Mage::getModel('slink/saasu_item');
			
            /* Look for item if UID available */
            if($this->getData('entity_uid')<>"" && $this->getData('updated_uid')<>"") $slink_item->get($this->getData('entity_uid'));

            /* Look for item by SKU if not able to get by UID */
            if($slink_item->getData('uid')=="" && $slink_item->getData('lastUpdatedUid')=="") $slink_item->getBySku($item->getSku());
			
			/* If Item is Combo, do not update */
			if($slink_item->getData('is_combo')) {
				$this->setData('transferred', date('Y-m-d H:i:s', time()));
			}

			$config = Mage::getStoreConfig('slinksettings/items');
			
            /* Post if not able to get by SKU */
            if($config['enableCreateUpdate']){
				if(($slink_item->getData('uid')=="" && $slink_item->getData('lastUpdatedUid')=="")
				   || /* If item has been updated - post anyway */
				   $this->getData('transferred') < $item->getData('updated_at')) {
                
					$slink_item->setItem( $item );


					/* Default Supplier */
					if($slink_item->getData('uid')=="" && $slink_item->getData('lastUpdatedUid')==""){
						/* Creating new item... */
						if(isset($config['defaultSupplierUid']) && $config['defaultSupplierUid']){
							$slink_item->setData('primarySupplierContactUid', $config['defaultSupplierUid']);
						}
						
						/* Item is Enabled */
						if(!$slink_item->getData('isActive')){
							$isActive = ($item->getData('status')==1 ? 'true' : 'false');
							$slink_item->setData('isActive', $isActive);
						}
						
						/* Set default notes */
						if(!$slink_item->getData('notes') && isset($config['items']['itemNotes']) && $config['items']['itemNotes']<>"")
							$slink_item->setData('notes', $config['items']['itemNotes']);
						
						
						/* Set is inventoried */
						if(!$slink_item->getData('isInventoried')){
							
							/* Set Stock levels if required */
							$item_stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($item);
							$isInventoried = ($item_stock->getData('manage_stock') ? 'true':'false');
							
							$slink_item->setData('isInventoried', $isInventoried);
							
							if($isInventoried=='true') {
								if(!$slink_item->getData('assetAccountUid')) $slink_item->setData('assetAccountUid', $slink_item->getData('_defaultAssetAccountUid'));
								if(!$slink_item->getData('saleCoSAccountUid')) $slink_item->setData('saleCoSAccountUid', $slink_item->getData('_defaultCoSAccountUid'));
							}
						}
						
						/* Set Buy Info */
						if($slink_item->getData('isBought')==""){
							$slink_item->setData('isBought', ($slink_item->getData('_defaultExpenseAccountUid')<>"" ? 'true' : 'false'));
							if($isInventoried=='false' && !$slink_item->getData('purchaseExpenseAccountUid')) $slink_item->setData('purchaseExpenseAccountUid', $slink_item->getData('_defaultExpenseAccountUid'));
						}
						
						/* Set Sell Info */
						if($slink_item->getData('isSold')==""){
							$slink_item->setData('isSold', ($slink_item->getData('_defaultIncomeAccountUid')<>"" ? 'true' : 'false'));
							if(!$slink_item->getData('saleIncomeAccountUid')) $slink_item->setData('saleIncomeAccountUid', $slink_item->getData('_defaultIncomeAccountUid'));
						}
						
						/* Update buying price if 'cost' assigned and item isBought */
						if($slink_item->getData('isBought')=='true' && $item->getData('cost')<>""){
							$slink_item->setData('buyingPrice', $item->getData('cost'));
						}
						
						/* Update selling price if 'price' assigned and item isSold */
						if($slink_item->getData('isSold')=='true' && $item->getPrice()<>""){
							$slink_item->setData('sellingPrice' , $item->getPrice());
						}
						
						/* Set Tax codes */
						if(!$slink_item->getData('purchaseTaxCode')) $slink_item->setData('purchaseTaxCode', $slink_item->getData('_defaultPurchaseTaxCode'));
						if(!$slink_item->getData('saleTaxCode')) $slink_item->setData('saleTaxCode', $slink_item->getData('_defaultSaleTaxCode'));
					}


					if($slink_item->post()) $slink_item->setData('transferred' , Mage::getModel('core/date')->gmtTimestamp());
			   }
			}elseif(!$slink_item->getData('uid') && !$slink_item->getData('lastUpdatedUid')) throw new Exception('Item not created/updated. Enable Create/Update is disabled. See System > Configuration > Slink > Settings. ');
            
            $errors = $slink_item->getError();
			if(count($errors)==0 && ($uid = $slink_item->getData('uid')) && ($lastUpdatedUid = $slink_item->getData('lastUpdatedUid'))){
				$this->addData(array('entity_uid'=>$uid,
									 'updated_uid'=>$lastUpdatedUid,
									 'transferred'=>$slink_item->getData('transferred')
                                     ));
                
                if(!$this->getData('transferred'))
                    $this->setData('transferred', Mage::getModel('core/date')->gmtTimestamp());
                
				$this->save();
                return true;
			}else{
				foreach($errors as $error){
					$message .= 'Item '.$item->getData('sku').' ( ID '.$item->entity_id.' ) - '.$error['code']. ' - '.$error['message'].' ( '.$error['action'].' ) <br>';
				}
                if($message<>"") throw new Exception( $message );
                return false;
			}            
		}else return false;
		

	}
	public function unlink(){
		if($this->getData('id') > 0) {
			$this->addData(array('entity_uid'=>'',
								 'updated_uid'=>'',
								 'transferred'=>''));
			$this->save();
			return;
		}else throw new Exception ('No Item with ID '.$this->getId());		
	}	
}