<?php
/**
 * Observer Model
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Sales
 * @package    Slink
 * @copyright  Copyright (c) 2012 Saaslink
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard@saaslink.com
 */

class Slink_Magesaasu_Model_Contacts_Observer{

	/* Create slink contact when creating new Magento Customer */
    public function create($observer){
        $event = $observer->getEvent();
        $customer = $event->getCustomer();
        $config = Mage::getStoreConfig('slinksettings');

        try{
			$slink_contact = Mage::getModel('slink/contacts')->load($customer->getId(), 'vid');
			if(!$slink_contact->getId()){
				$slink_contact->setData('vid', $customer->getId());
				$slink_contact->save();
			}
        }catch(Exception $e){
            throw new Exception ($e->getMessage());
        }
        
    }
    /* Delete addresses only if Magento Customer is deleted. */
    public function delete($observer){
        $event = $observer->getEvent();
        $customer = $event->getCustomer();
        
        try{
			$slink_contact = Mage::getModel('slink/contacts')->load($customer->getId(), 'vid');
			if($id = $slink_contact->getId()){
				$slink_contact->setId($id)->delete();
			}
        }catch(Exception $e){
            throw new Exception ($e->getMessage());
        }
    }
    
}
