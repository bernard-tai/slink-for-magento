<?php
/**
 * Slink for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Slink_MageSaasu
 * @package    Contacts
 * @copyright  Copyright (c) 2009 Bernard Tai
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard.tai@saaslink.net
 */

class Slink_MageSaasu_Model_Contacts extends Mage_Core_Model_Abstract
{
	
    protected function _construct()
    {
		parent::_construct();
        $this->_init('slink/contacts');		
    }

	public function link(){

		$customer = Mage::getModel('customer/customer')->load($this->getData('vid'));
        
		if($customer->getId()>0){
		
			$c = Mage::getModel('slink/saasu_contact');
		
            if($this->getData('entity_uid')<>"" && $this->getData('updated_uid')<>"") $c->get($this->getData('entity_uid'));

            if($c->getData('uid')=="" && $c->getData('lastUpdatedUid')==""){
				
				$billing = $customer->getDefaultBillingAddress();
				
				if($billing && $billing->getId()){
					$firstname = $billing->getData('firstname');
					$lastname = $billing->getData('lastname');
					$telephone = $billing->getData('telephone');
				}else{
					$firstname = $customer->getData('firstname');
					$lastname = $customer->getData('lastname');
				}
				
				$email = $customer->getData('email');
				
				$c->getByInfo($firstname, $lastname, $telephone, $email);
			}
			
            if(($c->getData('uid')=="" && $c->getData('lastUpdatedUid')=="")
               || $this->getData('transferred') < $customer->getData('updated_at')) {

				$config = Mage::getStoreConfig('slinksettings/contacts') ;

				$billing = $customer->getDefaultBillingAddress();
				$shipping = $customer->getDefaultShippingAddress();

				if($billing && $billing->getId()){
					$c->setContact($billing);
					$c->addPostalAddress($billing->getData('street'),
										 $billing->getData('city'),
										 $billing->getData('region'),
										 $billing->getData('postcode'),
										 $billing->getData('country_id'));
				}else{
					$c->setContact($customer);
				}
				
				if($shipping && $shipping->getId()){
					$c->addOtherAddress($shipping->getData('street'),
										$shipping->getData('city'),
										$shipping->getData('region'),
										$shipping->getData('postcode'),
										$shipping->getData('country_id'));
				}
				
                $c->setData('tags', $config['contactTags']);
				$c->setData('email', $customer->getData('email'));
				
                if($c->post()){
                    $c->setData('transferred' , Mage::getModel('core/date')->gmtTimestamp());
                }
			}
            
            $errors = $c->getError();
            if(count($errors)==0 && ($entity_uid = $c->getData('uid')) && ($updated_uid = $c->getData('lastUpdatedUid'))){
                $this->addData(array('entity_uid'=>$entity_uid,
                                     'updated_uid'=>$updated_uid,
                                     'transferred'=>$c->getData('transferred'))
                               );
                
                if(!$this->getData('transferred'))
                    $this->setData('transferred', Mage::getModel('core/date')->gmtTimestamp());
                
                $this->save();
                return true;
            }else{
                foreach($errors as $error){
                    $message .= 'Contact '.$customer->getData('lastname').' ( '.$error['code']. ' - '.$error['message'].' ( '.$error['action'].' ) <br>';
                }
                if($message<>"") throw new Exception($message);
                return false;
            }
			
		} else return false;
		
	}
	public function unlink(){
		if($this->getId()){
			$this->addData(array('entity_uid'=>'',
								 'updated_uid'=>'',
								 'transferred'=>''));
			$this->save();
			return;
		}else throw new Exception ('Invalid contact ID '.$this->getId());
	}
}