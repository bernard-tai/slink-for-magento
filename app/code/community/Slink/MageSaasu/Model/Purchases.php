<?php
/**
 * Slink for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category   Slink_MageSaasu
 * @package    Sales
 * @copyright  Copyright (c) 2009 Bernard Tai
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Bernard Tai bernard.tai@saaslink.net
 */

class Slink_MageSaasu_Model_Purchases extends Slink_MageSaasu_Model_Sales
{
	protected $purchase; /* variable for use with internal functions */
	protected $slink_saasu_purchase;	/* variable for use with internal functions */
	protected $linkMessage = '';
    
    protected function _construct(){
		parent::_construct();
        $this->_init('slink/sales');		

    }
	public function link(){
		$this->linkMessage = ''; 

        $this->purchase = Mage::getModel('sales/order')->load($this->getData('vid'));
        $purchase = &$this->purchase;
        
        if($purchase->getId()>0){

			$config = Mage::getStoreConfig('slinksettings') ;
			
			if(!$config['purchases']['enableCreatePurchaseOrder']){
				throw new Exception('Create Purchase Order not enabled. See Slink > Settings > Purchases.');
				return false;
			}
			
			$this->slink_saasu_purchase = Mage::getModel('slink/saasu_purchase');
            $slink_saasu_purchase = &$this->slink_saasu_purchase;

			/* Look for Magento Order increment id (if not Auto Numbered) */
			if($slink_saasu_purchase->getData('uid')=="" && $slink_saasu_purchase->getData('lastUpdatedUid')==""){
				if($this->getData('invoice_number')){
					$slink_saasu_purchase->getByPurchaseOrderNumber($this->getData('invoice_number'));
				}else {
					throw new Exception('No linked sale order / invoice found. Cannot create Purchase Order without corresponding Sale Order.');
					return false;
				}
			}

            if(($slink_saasu_purchase->getData('uid')=="" && $slink_saasu_purchase->getData('lastUpdatedUid')=="") 
               || /* If item has been updated - post anyway */
               $this->getData('transferred') < $purchase->getData('updated_at')){
                
                $slink_saasu_purchase->setPurchase( $purchase );

				if(!$slink_saasu_purchase->getData('status')) $slink_saasu_purchase->setData('status', $config['purchases']['saasuPurchaseStatus']);
				
				$slink_saasu_purchase->setData('layout', $config['purchases']['purchaseLayout']);
                /* if Layout=S or I determine how to add items here */
			
				$slink_saasu_purchase->setData('purchaseOrderNumber', $this->getData('invoice_number'));
                $slink_saasu_purchase->setData('tags', $config['sales']['saleTags']);
                $slink_saasu_purchase->setData('ccy', $purchase->getData('order_currency_code'));
                /* Set quick payment bank account here */
                
                $this->_setContacts();
                $this->_setItems();
				
				/* Set Payment Disabled - never create payments for Purchase Order */
				// $this->_setPayments();

                if($slink_saasu_purchase->post()){
                    $slink_saasu_purchase->setData('transferred' , Mage::getModel('core/date')->gmtTimestamp());
                }		                
            }
			
			if($errors = $slink_saasu_purchase->getError()){
				foreach($errors as $error){
					$this->linkMessage .= 'Sale '.$purchase->getData('increment_id').' - '.$error['code']. ' - '.$error['message'].($error['action'] ? ' ('.$error['action'].' )' : '').'<br>';
				}
			}					
			if($this->linkMessage) throw new Exception($this->linkMessage);			
			return true;
			
		}else return false;
				
	}
	
	private function _setContacts(){
		$config = Mage::getStoreConfig('slinksettings') ;
		$slink_saasu_purchase = &$this->slink_saasu_purchase;
		if($slink_saasu_purchase){
			if($billing_uid = $config['purchases']['defaultBillingSupplier']){
				$slink_saasu_purchase->setData('contactUid', $billing_uid);
			}
			
			if($config['purchases']['enableDropShip']){
				$slink_saasu_sale = Mage::getModel('slink/saasu_sale');
				$slink_saasu_sale->getByInvoiceNumber($this->getData('invoice_number'));

				if($slink_saasu_sale->getData('uid')<>""){
					$slink_saasu_purchase->setData('shipToContactUid', $slink_saasu_sale->getData('shipToContactUid'));
				}else {
					Mage::getsingleton('adminhtml/session')->addError('Ship to contact not found for '.$this->getData('invoice_number'));
				}
			}

		}
	}
	
	private function _setItems(){
        $purchase = &$this->purchase;
        $slink_saasu_purchase = &$this->slink_saasu_purchase;

		if($purchase->getId()>0 && $slink_saasu_purchase){
			$config = Mage::getStoreConfig('slinksettings') ;
			$items = $purchase->getItemsCollection();
            
			foreach($items as $item){
			
				/**	Price info in Configurable product,
				 Qty info in Configurable and its Simple product */
				if($item->product_type == 'configurable'){
					continue;
				}
				/** Skip Bundle products. Price and Qty info in its Simple product */
				elseif($item->product_type == 'bundle'){
					continue;
				}else{
				
					if($item->parent_item_id > 0){ // Has a parent configurable product, use parent's price and qty info
						$parent = Mage::getModel('sales/order_item')->load($item->parent_item_id);
						if($parent->getId()>0 && $parent->product_type=='configurable') $i = $parent;
						else $i = $item;
					}else $i = $item;
					
					/* Grand Cucina */
					$product = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($item->getData('sku')));
					
					if( $product->getData('grandcucina_buy_unit_qty')>1){
						$qty = $item->getData('qty_ordered') * $product->getData('grandcucina_buy_unit_qty');
						
					}else{
						$qty = $item->getData('qty_ordered');
					}
					/* End */
					
					if($product_id = Mage::getModel('catalog/product')->getIdBySku($item->getData('sku'))){
                        if(($slink_item = Mage::getModel('slink/items')->load($product_id, 'vid')) && !$slink_item->getId()){
                            $slink_item->setData(array('vid'=>$product_id));
                            $slink_item->save();
						}
                        $slink_item->link(); /* Link to ensure correct uid */
						$slink_saasu_item = Mage::getModel('slink/saasu_item');
						$slink_saasu_item->getBySku($item->getSku());
					}else{
						/* Item not found in catalog. Find item in Slink and link by SKU, else create */
						$slink_saasu_item = Mage::getModel('slink/saasu_item');
                        $slink_saasu_item->getBySku($i->getSku());
                        
						if(!$slink_saasu_item->getData('uid')) throw new Exception('Sale '.$this->sale->getData('increment_id').' - '.
																				   'Item '.$item->getSku().' not found.');
                        
                        $entity_uid = $slink_saasu_item->getData('uid');
					}
				
					$uid = $slink_saasu_item->getData('uid');
					$slink_saasu_item->get($uid);
					
					if($slink_saasu_item->getData('isBought')=="true"){
						
						$unitPriceInclTax = $slink_saasu_item->getData('buyingPrice');
						$purchase_tax_code = $slink_saasu_item->getData('purchaseTaxCode');
						
						if($slink_saasu_item->getData('isBuyingPriceIncTax')=="false"){
							if($purchase_tax_code=="G11"){ /* Aust GST Purchase code */
								$unitPriceInclTax = $unitPriceInclTax * 1.1;
							}else{
								Mage::getSingleton('adminhtml/session')->addError("Warning: No tax info for item ".$slink_saasu_item->getData('code') ." (order ".$slink_saasu_purchase->getData('purchaseOrderNumber')."). Setting to $0.00...");
								$unitPriceInclTax = 0;
							}
						}

						$slink_saasu_purchase->addItemInvoiceItem($qty,
																  $uid,
																  $item->getData('name'),
																  $purchase_tax_code, //tax code
																  //											 ($discount > 0 ? ($unitPriceInclTax / (1 - $discount)) : $unitPriceInclTax),
																  $unitPriceInclTax,
																  //											 $item->getData('discount_percent'));
																  0);
					}else continue; /* Item is not Bought. Ignore in Purchase Order */
				}
			}
		}
	}	
}